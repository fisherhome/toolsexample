#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSObject (Associate)

- (id)associatedObjectForKey:(NSString *)key;
- (void)associatedObject:(id)value forKey:(NSString *)key;
@end

NS_ASSUME_NONNULL_END
