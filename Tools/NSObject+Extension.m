#import "NSObject+Extension.h"
#import <objc/runtime.h>

@implementation NSObject (Extension)

- (id)associatedObjectForKey:(NSString *)key {
    if (key) {
        @synchronized (self.runtimeDict) {
            return self.runtimeDict[key];
        }
    }
    return nil;
}

- (void)associatedObject:(id)value forKey:(NSString *)key {
    if (key) {
        @synchronized (self.runtimeDict) {
            self.runtimeDict[key] = value;
        }
    }
}
- (NSMutableDictionary *)runtimeDict {
    NSMutableDictionary *dict = objc_getAssociatedObject(self, _cmd);
    if (!dict) {
        dict = [NSMutableDictionary dictionary];
        objc_setAssociatedObject(self, _cmd, dict, OBJC_ASSOCIATION_RETAIN);
    }
    return dict;
}

@end
