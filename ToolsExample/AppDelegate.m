//
//  AppDelegate.m
//  ToolsExample
//
//  Created by fisherman on 2022/1/13.
//

#import "AppDelegate.h"
#import "Tools.h"
@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [self associatedObject:@"虞嘉伟" forKey:@"name"];
    return YES;
}

@end
