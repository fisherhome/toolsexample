//
//  Tools.h
//  Tools
//
//  Created by fisherman on 2022/1/13.
//

#import <Foundation/Foundation.h>
#import "NSObject+Extension.h"

/// block声明时参数不填，传值时可传任意参数
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wstrict-prototypes"
typedef void(^callback_block_t)();
#pragma clang diagnostic pop


/**
 ClassAnnotation(ViewController, UITableViewDelegate, UITableViewDataSource)
 给ViewController类添加两个协议
 */
#define ClassAnnotation(clz, protocol...) \
@interface clz () <protocol> \
@end
