//
//  ViewController.m
//  ToolsExample
//
//  Created by fisherman on 2022/1/13.
//

#import "ViewController.h"
#import "Tools.h"
#import <objc/runtime.h>
#import "AppDelegate.h"

ClassAnnotation(ViewController, UITableViewDelegate)
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    AppDelegate *delegate = (AppDelegate*)UIApplication.sharedApplication.delegate;
    NSString *name = [delegate associatedObjectForKey:@"name"];
    NSLog(@"--name: %@", name);
    
    callback_block_t block = ^(NSString *obj1, NSString *obj2) {
        //obj1 = 数字
        //obj2 = 字符串
        NSLog(@"obj1 = %@\nobj2 = %@", obj1, obj2);
    };
    block(@"数字", @"字符串");
}


@end
